const webpack = require('webpack');
const path = require('path');
const Merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const CommonConfig = require('./webpack.common.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = Merge(CommonConfig, {
    entry: {
        app: './src/index.jsx'
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: false,
            debug: false
        }),
        new UglifyJsPlugin(),
        new Dotenv({
            path: './.env'
        })
    ],
    output: {
        path: path.join(__dirname, '../public/dist'),
        filename: 'lib.min.js',
        library: 'lib.min',
        libraryTarget: 'umd',
        umdNamedDefine: true,
        sourceMapFilename: 'karmamodules.map'
    },
    module: {
        rules: [
            {
                test: [/\.js?$/, /\.jsx?$/],
                loaders: ['babel-loader'],
                exclude:/(node_modules)/,
                include: path.join(__dirname, '../src')
            },
            {
                test: /\.css$/,
                use: ['to-string-loader', 'css-loader']
            },
            {
                test: /\.(jpg|png|gif)$/,
                use: 'file-loader'
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 100000
                    }
                }
            }
        ]
    },
})

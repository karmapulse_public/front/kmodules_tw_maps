/*
*   VISUALIZACIÓN TWEET MAPS
*  --------------------------
*   Este componente renderea la data que le ha llegado.
*   En la mayoría de los casos, es un componente stateless.
*/
// import 'mapbox-gl/dist/mapbox-gl.css';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MapGL from 'react-map-gl';
import DeckGL, { ScreenGridLayer } from 'deck.gl';
import { css } from 'glamor';

import mapboxStyles from './mapbox-gl';

css.insert(mapboxStyles);

const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoiZ2FyZmlhc2xvcGV6IiwiYSI6ImNqY256OHB4NjFvcHEycG1yMGJkZmRlbzMifQ.wK2AyBjgxdqvbxbloe_gjg';

class TweetMapGrid extends Component {
    constructor(props) {
        super(props);
        this.defaultZoom = 11;
        this.mapConfig = {
            key: 'AIzaSyBArcwzTXl5DINP26zbIEv0a_kclyXykyo',
            language: 'mx',
            region: 'mx'
        };
        this.state = {
            viewport: {
                width: 700,
                height: 700,
                latitude: 37.7577,
                longitude: -122.4376,
                zoom: 10
            }
        };
        this.onViewportChange = this.onViewportChange.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.geohashSelected = this.geohashSelected.bind(this);
        this.updateCoords = this.updateCoords.bind(this);

        this.zoomed = {};
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        if (this.props.config.options.withLocalization) {
            if (window.navigator.geolocation) {
                window.navigator.geolocation.getCurrentPosition(this.updateCoords);
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data,
            config: nextProps.config
        })
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    onViewportChange(viewport) {
        if (viewport.zoom >= 12) {
            if (this.zoomed['12'] === undefined) {
                this.zoomed['12'] = true;
                this.props.updateData({
                    precision: {
                        value: 8
                    }
                });
            }
        }
        if (viewport.zoom >= 13) {
            if (this.zoomed['13'] === undefined) {
                this.zoomed['13'] = true;
                this.props.updateData({
                    precision: {
                        value: 12
                    }
                });
            }
        }

        this.setState({
            viewport: { ...this.state.viewport, ...viewport }
        });
    }

    updateWindowDimensions() {
        this.setState({
            viewport: {
                ...this.state.viewport,
                ...{ width: window.innerWidth, height: window.innerHeight
                }
            }
        });
    }

    updateCoords(Position) {
        this.props.updateData({
            geo: {
                value: {
                    lat: Position.coords.latitude,
                    lon: Position.coords.longitude
                }
            }
        });
        const newVp = this.state.viewport;
        newVp.latitude = Position.coords.latitude;
        newVp.longitude = Position.coords.longitude;
        this.setState({
            viewport: newVp
        });
    }

    geohashSelected(obj) {
        this.props.onClusterSelected(obj);
    }

    render() {
        if (this.props.data.data.length > 0) {
            return (
                <MapGL
                    {...this.state.viewport}
                    mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}
                    onViewportChange={this.onViewportChange}
                >
                    <DeckGL
                        {...this.state.viewport}
                        layers={[
                            new ScreenGridLayer({
                                id: 'screen-grid-layer',
                                minColor: [0, 0, 0, 0],
                                getPosition: d => d,
                                getWeight: (w) => {
                                    console.log("getWeight:");
                                    console.log(w);
                                },
                                data: this.props.data.data,
                                cellSizePixels: 40
                            })
                        ]}
                    />
                </MapGL>
            );
        }
        return (
            <div>
                Getting zones...
            </div>
        );
    }
}

TweetMapGrid.defaultProps = {

};

TweetMapGrid.propTypes = {
    config: PropTypes.object,
    data: PropTypes.object,
    updateData: PropTypes.func,
    onClusterSelected: PropTypes.func
};

const LocationIcon = ({ total }) => (
    <svg
        width="35px"
        height="46px"
        viewBox="0 0 35 46"
        version="1.1"
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
            <path
                d="M17.5 0C7.825 0 0 7.199 0 16.1 0 28.175 17.5 46 17.5 46S35 28.175 35 16.1C35 7.199 27.175 0 17.5 0z"
                fill="#000"
                fillRule="nonzero"
            />
            <text
                fontFamily="Roboto-Regular, Roboto"
                fontSize={9}
                fontWeight="normal"
                fill="#FFF"
                textAnchor="middle"
            >
                <tspan x="50%" y="50%">
                    {total}
                </tspan>
            </text>
            <polygon points="0 0 35 0 35 46 0 46" />
        </g>
    </svg>
);

export default TweetMapGrid;

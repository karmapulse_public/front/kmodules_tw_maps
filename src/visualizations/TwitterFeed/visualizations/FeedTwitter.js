import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import TwitterCard from './customComponents/TwitterCard';
import Gallery from './customComponents/TwitterGallery';
import styles from './FeedTwitterStyles';
import TrumpetIcon from '../../../helpers/icons/trumpetIcon';

const propTypes = {
    scroll: PropTypes.func,
    moduleConfig: PropTypes.object,
    tweets: PropTypes.array,
    moduleColor: PropTypes.string,
    miniMode: PropTypes.bool
};

const defaultProps = {
    scroll: 'hey',
    moduleConfig: {},
    tweets: [],
    moduleColor: '#666',
    miniMode: false
};

class FeedTwitter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: moduleData.tweets,
            scrollActive: true
        };
    }

    componentDidMount() {
        const feed = ReactDOM.findDOMNode(this);
        const feedTweets = feed.getElementsByClassName('feed-container-list')[0];
        const isUserScrollingDown = positions => (
            positions[0].sT < positions[1].sT
        );

        const isScrollExpectedPercent = (position, percent) =>
            ((position.sT + position.cH) / position.sH) > (percent / 100)
        ;
    }

    renderTweet(tweet, index) {
        return (
            <TwitterCard {...tweet} key={index}>
                <Gallery items={tweet._source.images} />
            </TwitterCard>
        );
    }

    render() {
        const { moduleConfig, moduleColor, miniMode } = this.props;
        const { tweets } = props;
        const { title = 'FEED DE TWEETS' } = moduleConfig;

        return (
            <div className="feed-twitter" {...styles(moduleColor)}>
                <div className="feed-label">
                    <h1>
                        <TrumpetIcon color="#FFF" />
                        {title}
                    </h1>
                </div>
                <div className="feed-container-list">
                    {(() => {
                        if (miniMode) {
                            return this.renderTweet(tweets[0], 0);
                        }
                        return tweets.map(
                            (tweet, index) => (this.renderTweet(tweet, index))
                        );
                    })()}
                </div>
            </div>
        );
    }
}

FeedTwitter.propTypes = propTypes;
FeedTwitter.defaultProps = defaultProps;

export default FeedTwitter;

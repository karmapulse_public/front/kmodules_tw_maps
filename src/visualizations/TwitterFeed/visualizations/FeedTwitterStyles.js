import { css } from 'glamor';

const styles = color => (
    css({
        ' .feed-label': {
            maxWidth: 590,
            backgroundColor: color === undefined ? '#666' : color,
            margin: '0 auto 10px',
            padding: 20,
            ' >h1': {
                color: '#fff',
                ' svg': {
                    marginRight: 10
                }
            },
            '@media screen and (min-width:1280px)': {
                maxWidth: 590
            }
        },

        ' .feed-container-list': {
            maxWidth: 590,
            width: '100%',
            height: 'calc(100vh - 225px)',
            overflowY: 'auto',
            columnCount: 'initial !important',
            WebkitOverflowScrolling: 'touch',
            margin: '0 auto',
            color: 'rgba(0,0,0, 0.5)',
            ' >div': {
                maxWidth: 590,
                margin: '0 auto'
            },
            '@media screen and (min-width:1280px)': {
                maxWidth: 1200,
                columnCount: 2,
                '>div': {
                    breakInside: 'avoid'
                }
            }
        }
    })
);

export default styles;

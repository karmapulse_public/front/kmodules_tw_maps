import { css } from 'glamor';

const styles = (n) => {
    let s = [];
    const collect = {};

    for (let i = 1; i <= n; i += 1) {
        const figure = { [` figure:nth-child(${i})`]: {
            width: (n === 1)
                ? ('100%')
                : ((i === 1) ? `${100 - (100 / n)}%` : `${100 / n}%`),
            height: (i === 1) ? '100%' : `${100 / (n - 1)}%`,
            boxSizing: 'border-box',
            float: (i === 1) ? 'left' : 'right',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            border: '1px solid #ffffff'
        } };

        s = s.concat(figure);
    }
    s.map(
        (e, i) => {
            collect[` figure:nth-child(${i + 1})`] = e[` figure:nth-child(${i + 1})`];
        }
    );

    return css(collect, {
        width: '100%',
        height: 378,
        marginBottom: 15,
        overflowY: 'hidden'
    });
};
export default styles;

import { css } from 'glamor';

const styles = () => (
    css({
        ' .twitter-card': {
            backgroundColor: '#ffffff',
            boxShadow: '0 1px 8px 0 rgba(0, 0, 0, 0.2)',
            width: '100%',
            maxWidth: 590,
            minWidth: 300,
            margin: '0px auto 5px',
            position: 'relative',
            fontSize: 10,

            '&__user-image': {
                position: 'absolute',
                top: 20,
                left: 20,
                width: 48,
                height: 48,
                borderRadius: '2px',
            },

            '&__container': {
                padding: 20,
                width: '100%',
                boxSizing: 'border-box'
            }
        },
        ' .container': {

            '&__header': {
                height: 48,
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
                marginBottom: 10,
                paddingLeft: 58,

                ' > div': {

                    ' >a': {
                        display: 'block',
                        ' >svg': {
                            marginLeft: 5,
                            verticalAlign: 'middle'
                        }
                    }
                },
                '&__name': {
                    fontSize: '1.4em',
                    fontWeight: 500,
                    color: '#000',
                    textDecoration: 'none',
                },

                '&__username': {
                    marginTop: 3,
                    fontSize: '1.2em',
                    fontWeight: 500,
                    color: '#000',
                    opacity: 0.54,
                    textDecoration: 'none',
                },

                '&__datetime': {
                    fontSize: '1.2em',
                    fontWeight: 500,
                    color: '#000',
                    opacity: 0.54,
                    textDecoration: 'none',
                    '&:nth-of-type(2)': {
                        marginTop: 3,
                        textAlign: 'right',
                    }
                },
            },

            '&__sub-header': {
                marginBottom: 10,

                ' > div': {
                    display: 'flex',
                    alignItems: 'baseline',
                    marginRight: 20,
                },

                '&__kloutscore': {

                    ' h5': {
                        fontSize: '1.4em',
                        fontWeight: 500,
                        color: '#d85624',
                    },

                    ':before': {
                        content: '""',
                        width: 16,
                        height: 13,
                        marginRight: 5,
                        backgroundRepeat: 'no-repeat',
                        backgroundImage: 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAANCAYAAACgu+4kAAAAAXNSR0IArs4c6QAAAklJREFUKBV9UztoVEEUPTPzfuuuUROySXY3WcMmWcFGF4M2gkGbINj5aRQtgtjZqQRtLEQiop0kvaiNCDaiIGLjpzBE/GBCAiK7GDeR/Hy7772Z8b55W1hILrzhztx7z5y55z42e3KgpMFeMK13SmgAjL6WaUUO7dm/ZxqCtlTzQzJ12JIar2zbykfgYIogojApYBzcS1Omgmr6CSLFmZuCthwILQuq4b/kAjovvQzSB0axZf8oeLoNOgzg9BRRvPUU+UtTYFQQA1vZAvquP0LxxmPw7n5wFeU4YUKt1BFWFyC2d8It7zMMGLfgdBZgdfQQYJMYaGTPXoPXvxvr75+jMTsNRTmW4UZ0m/MfIdq7YHf1wR2qgHEOLQnecZGujCA1uAdbh4/A//YB9Yd3wERS2gKgrkQBmnPThoU3uBdqY4VoB7AItHB5ytyjlUL9wW1IinHqRWzcrMbjsHMlgxxW5yGXasZXG6v4dX8Ca2+fGVZtI8fBbIdeFCvWAtAygt1bhtM7BPl7Ef7Ma8jVJUAIYrKG5SeTqN29CH9uBtsOHsOOo+eggkYLgGSy2rvh7RqGpkP/8xujAhM2yU/Suh7JmYHy1/Hz3hWiv4rsmXFkKocSZRxBzaIWBF/eJe+mmzlRlMs1LE6OQzX+UH+aEI6HYOETqhPn4eQHSCkCp0azrydKNy3osUhpYSaOAmYYlSZGNEBxIhUbiycwCGyXqVTIaIJilnHg+6lyLkAkkqzN11DrFNPsNOf8gs3QYQA2L/l/NP6HlGZX/wJMX+J51dytqgAAAABJRU5ErkJggg==)',
                        backgroundPosition: 'center center',
                    }
                },

                '&__followers': {

                    ' h5': {
                        fontSize: '1.4em',
                        color: '#000',
                        '>svg': {
                            marginRight: 5,
                            verticalAlign: 'middle'
                        }
                    },
                },
            },

            '&__body': {
                fontSize: '1.4em',
                fontWeight: 400,
                color: 'rgba(0, 0, 0, 0.84)',
                lineHeight: 1.29,
                marginBottom: 20,
                wordWrap: 'break-word',

                ' > em': {
                    backgroundColor: '#FFEBB9',
                },
                ' > a': {
                    color: '#5c6bc0',
                    textDecoration: 'none',
                },
            },

            '&__images': {
                width: '100%',
                marginBottom: 10
            },

            '&__footer': {
                display: 'flex',
                alignItems: 'flex-end',
                justifyContent: 'space-between',

                '&__reactions': {
                    display: 'flex',
                    alignItems: 'center',
                    ' >a': {
                        marginRight: 20
                    },

                    ' > div': {
                        marginRight: 15,

                        ' h5': {
                            fontSize: '1.4em',
                            color: '#5c6bc0',
                            marginBottom: 2,
                        },
                        ' h4': {
                            fontSize: '1.6em',
                            fontWeight: 500,
                            color: '#000',
                        },
                    }
                },

                '&__sentiment': {
                    display: 'flex',
                    alignItems: 'center',

                    ' h5': {
                        fontSize: '1.2em',
                        fontWeight: 500,
                    },

                    '&__oval': {
                        width: 10,
                        height: 10,
                        marginRight: 5,
                        borderRadius: '50%',
                    },

                    ' .oval-Positivo': {
                        backgroundColor: '#4caf50',
                    },

                    ' .oval-Neutral': {
                        backgroundColor: '#607d8b',
                    },

                    ' .oval-Negativo': {
                        backgroundColor: '#f44336',
                    },

                    ' .text-Positivo': {
                        color: '#4caf50',
                    },

                    ' .text-Neutral': {
                        color: '#607d8b',
                    },

                    ' .text-Negativo': {
                        color: '#f44336',
                    }
                }
            }
        }
    })
);

export default styles;

import React from 'react';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import moment from 'moment';

import { numberWithCommas } from '../../../../../helpers/number';
import formatBodyText from '../../../../../helpers/formatBodyText';
import VerifiedIcon from '../../../../../helpers/icons/verifiedIcon';
import UserIcon from '../../../../../helpers/icons/userIcon';
import LikeIcon from '../../../../../helpers/icons/likeIcon';
import ReplyIcon from '../../../../../helpers/icons/replyIcon';
import RtIcon from '../../../../../helpers/icons/rtIcon';
import styles from './TwitterCardMobileStyles';

const propTypes = {
    _source: PropTypes.object,
    children: PropTypes.object
};

const defaultProps = {
    _source: {},
    children: {}
};

// const CardHorizontalBars = ({ moduleConfig, moduleData, moduleSide }) => {
const TwitterCardMobile = ({ _source, children }) => {
    const body = () => ({ __html: formatBodyText(_source.body) });
    const { user } = _source;
    const dateMoment = upperFirst(moment(_source.posted_time).format('MMM D, YYYY'));
    const dateHour = upperFirst(moment(_source.posted_time).format('HH:mm'));
    const followers = user.followers ? numberWithCommas(user.followers): 0;
    const verified = user.verified ? <VerifiedIcon /> : '';

    return (
        <div {...styles()}>
            <div className="twitter-card">
                <a
                    className="twitter-card__user-image"
                    href={user.link}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <img src={user.image} alt="Foto de usuario" />
                </a>
                <div className="twitter-card__container">
                    <div className="container__header">
                        <div>
                            <a
                                className="container__header__name"
                                href={user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {user.name}
                                {verified}
                            </a>
                            <a
                                className="container__header__username"
                                href={user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                @{user.username}
                            </a>
                        </div>
                        <div>
                            <a
                                className="container__header__datetime"
                                href={_source.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {dateMoment}
                            </a>
                            <a
                                className="container__header__datetime"
                                href={_source.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {dateHour}
                            </a>
                        </div>
                    </div>
                    <div className="container__sub-header">
                        <div className="container__sub-header__followers">
                            <h5>
                                <UserIcon color="#000" />
                                {followers}
                            </h5>
                        </div>
                    </div>
                    <div className="container__images">
                        {children}
                    </div>
                    <div className="container__body" dangerouslySetInnerHTML={body()} />
                    <div className="container__footer">
                        <div className="container__footer__reactions">
                            <a
                                href={`https://twitter.com/intent/tweet?in_reply_to=${_source.id}`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <ReplyIcon color="#bdbdbd" />
                            </a>
                            <a
                                href={`https://twitter.com/intent/retweet?tweet_id=${_source.id}`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <RtIcon color="#bdbdbd" />
                            </a>
                            <a
                                href={`https://twitter.com/intent/like?tweet_id=${_source.id}`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <LikeIcon color="#bdbdbd" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

TwitterCardMobile.propTypes = propTypes;
TwitterCardMobile.defaultProps = defaultProps;

export default TwitterCardMobile;

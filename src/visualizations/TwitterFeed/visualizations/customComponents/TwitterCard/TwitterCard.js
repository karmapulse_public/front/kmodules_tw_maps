import React, { PropTypes } from 'react';
import upperFirst from 'lodash/upperFirst';
import moment from 'moment';

import { nFormatter } from '../../../../../helpers/number';
import VerifiedIcon from '../../../../../helpers/icons/verifiedIcon';
import UserIcon from '../../../../../helpers/icons/userIcon';
import formatBodyText from '../../../../../helpers/formatBodyText';
import styles from './TwitterCardStyles';

const propTypes = {
    _source: PropTypes.object,
    children: PropTypes.object
};

const defaultProps = {
    _source: {},
    children: {}
};

// const CardHorizontalBars = ({ moduleConfig, moduleData, moduleSide }) => {
const TwitterCard = ({ _source, children }) => {
    const body = () => ({ __html: formatBodyText(_source.body) });
    const { user } = _source;
    const dateMoment = upperFirst(moment(_source.posted_time).format('MMM D, YYYY HH:mm'));
    const followers = nFormatter(user.followers);
    const sentiment = upperFirst(_source.classificators[0].tag);
    const verified = user.verified ? <VerifiedIcon /> : '';
    const retweets = () => {
        if (_source.retweets) {
            return (
                <div>
                    <h5>Retweets</h5>
                    <h4>{_source.retweets}</h4>
                </div>
            );
        }
        return '';
    };
    const likes = () => {
        if (_source.like) {
            return (
                <div>
                    <h5>Me gusta</h5>
                    <h4>{_source.like}</h4>
                </div>
            );
        }
        return '';
    };

    return (
        <div {...styles()}>
            <div className="twitter-card">
                <a
                    className="twitter-card__user-image"
                    href={user.link}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <img src={user.image} alt="Foto de usuario" />
                </a>
                <div className="twitter-card__container">
                    <div className="container__header">
                        <div>
                            <a
                                className="container__header__name"
                                href={user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {user.name}
                                {verified}
                            </a>
                            <a
                                className="container__header__username"
                                href={user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                @{user.username}
                            </a>
                        </div>
                        <a
                            className="container__header__datetime"
                            href={_source.link}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {dateMoment}
                        </a>
                    </div>
                    <div className="container__sub-header">
                        <div className="container__sub-header__kloutscore">
                            <h5>{user.klout}</h5>
                        </div>
                        <div className="container__sub-header__followers">
                            <h5>
                                <UserIcon color="#000" />
                                {followers}
                            </h5>
                        </div>
                    </div>
                    <div className="container__body" dangerouslySetInnerHTML={body()} />
                    {children}
                    <div className="container__footer">
                        <div className="container__footer__reactions">
                            { retweets() }
                            { likes() }
                        </div>
                        <div className="container__footer__sentiment">
                            <div className={`container__footer__sentiment__oval oval-${sentiment}`} />
                            <h5 className={`text-${sentiment}`}>{sentiment}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

TwitterCard.propTypes = propTypes;
TwitterCard.defaultProps = defaultProps;

export default TwitterCard;

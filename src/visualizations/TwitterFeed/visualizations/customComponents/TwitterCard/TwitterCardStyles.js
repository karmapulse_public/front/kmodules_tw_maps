import { css } from 'glamor';

const styles = () => (
    css({
        ' .twitter-card': {
            borderRadius: '2px',
            backgroundColor: '#ffffff',
            boxShadow: '0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.12)',
            width: '100%',
            maxWidth: 590,
            minWidth: 300,
            margin: '0px auto 15px',
            position: 'relative',

            '&__user-image': {
                position: 'absolute',
                top: 20,
                left: 20,
                width: 48,
                height: 48,
                borderRadius: '2px',
            },

            '&__container': {
                padding: '20px 20px 20px 78px',
                width: '100%',
                boxSizing: 'border-box'
            }
        },
        ' .container': {

            '&__header': {
                display: 'flex',
                justifyContent: 'space-between',
                marginBottom: 9,

                ' > div': {
                    display: 'flex',
                    alignItems: 'center',

                    ' >svg': {
                        marginLeft: 5,
                        verticalAlign: 'middle'
                    }
                },
                '&__name': {
                    fontSize: 14,
                    fontWeight: 500,
                    color: '#5c6bc0',
                    marginRight: 10,
                    textDecoration: 'none',
                    '>svg': {
                        marginLeft: 5,
                        verticalAlign: 'middle'
                    }
                },

                '&__username, &__datetime': {
                    fontSize: 12,
                    fontWeight: 500,
                    color: 'rgba(0, 0, 0, 0.54)',
                    textDecoration: 'none',
                },
            },

            '&__sub-header': {
                display: 'flex',
                alignItems: 'center',
                marginBottom: 8,

                ' > div': {
                    display: 'flex',
                    alignItems: 'center',
                    marginRight: 20,
                },

                '&__kloutscore': {

                    ' h5': {
                        fontSize: 14,
                        fontWeight: 500,
                        color: '#d85624',
                    },

                    ':before': {
                        content: '""',
                        width: 16,
                        height: 13,
                        marginRight: 5,
                        backgroundRepeat: 'no-repeat',
                        backgroundImage: 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAANCAYAAACgu+4kAAAAAXNSR0IArs4c6QAAAklJREFUKBV9UztoVEEUPTPzfuuuUROySXY3WcMmWcFGF4M2gkGbINj5aRQtgtjZqQRtLEQiop0kvaiNCDaiIGLjpzBE/GBCAiK7GDeR/Hy7772Z8b55W1hILrzhztx7z5y55z42e3KgpMFeMK13SmgAjL6WaUUO7dm/ZxqCtlTzQzJ12JIar2zbykfgYIogojApYBzcS1Omgmr6CSLFmZuCthwILQuq4b/kAjovvQzSB0axZf8oeLoNOgzg9BRRvPUU+UtTYFQQA1vZAvquP0LxxmPw7n5wFeU4YUKt1BFWFyC2d8It7zMMGLfgdBZgdfQQYJMYaGTPXoPXvxvr75+jMTsNRTmW4UZ0m/MfIdq7YHf1wR2qgHEOLQnecZGujCA1uAdbh4/A//YB9Yd3wERS2gKgrkQBmnPThoU3uBdqY4VoB7AItHB5ytyjlUL9wW1IinHqRWzcrMbjsHMlgxxW5yGXasZXG6v4dX8Ca2+fGVZtI8fBbIdeFCvWAtAygt1bhtM7BPl7Ef7Ma8jVJUAIYrKG5SeTqN29CH9uBtsOHsOOo+eggkYLgGSy2rvh7RqGpkP/8xujAhM2yU/Suh7JmYHy1/Hz3hWiv4rsmXFkKocSZRxBzaIWBF/eJe+mmzlRlMs1LE6OQzX+UH+aEI6HYOETqhPn4eQHSCkCp0azrydKNy3osUhpYSaOAmYYlSZGNEBxIhUbiycwCGyXqVTIaIJilnHg+6lyLkAkkqzN11DrFNPsNOf8gs3QYQA2L/l/NP6HlGZX/wJMX+J51dytqgAAAABJRU5ErkJggg==)',
                        backgroundPosition: 'center center',
                    }
                },

                '&__followers': {

                    ' h5': {
                        fontSize: 14,
                        fontWeight: 500,
                        color: '#000',
                        '>svg': {
                            marginRight: 5,
                            verticalAlign: 'middle'
                        }
                    }
                },
            },

            '&__body': {
                fontSize: 14,
                fontWeight: 400,
                color: 'rgba(0, 0, 0, 0.84)',
                lineHeight: '17px',
                marginBottom: 20,
                wordWrap: 'break-word',

                ' > em': {
                    backgroundColor: '#FFEBB9',
                },
                ' > a': {
                    color: '#5c6bc0',
                    textDecoration: 'none',
                },
            },

            '&__footer': {
                display: 'flex',
                alignItems: 'flex-end',
                justifyContent: 'space-between',

                '&__reactions': {
                    display: 'flex',
                    alignItems: 'center',

                    ' > div': {
                        marginRight: 15,

                        ' h5': {
                            fontSize: 14,
                            color: '#5c6bc0',
                            marginBottom: 2,
                        },
                        ' h4': {
                            fontSize: 16,
                            fontWeight: 500,
                            color: '#000',
                        },
                    }
                },

                '&__sentiment': {
                    display: 'flex',
                    alignItems: 'center',

                    ' h5': {
                        fontSize: 12,
                        fontWeight: 500,
                    },

                    '&__oval': {
                        width: 10,
                        height: 10,
                        marginRight: 5,
                        borderRadius: '50%',
                    },

                    ' .oval-Positivo': {
                        backgroundColor: '#4caf50',
                    },

                    ' .oval-Neutral': {
                        backgroundColor: '#607d8b',
                    },

                    ' .oval-Negativo': {
                        backgroundColor: '#f44336',
                    },

                    ' .text-Positivo': {
                        color: '#4caf50',
                    },

                    ' .text-Neutral': {
                        color: '#607d8b',
                    },

                    ' .text-Negativo': {
                        color: '#f44336',
                    }
                }
            }
        }
    })
);

export default styles;

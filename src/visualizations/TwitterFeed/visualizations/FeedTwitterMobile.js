import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import TwitterCardMobile from './customComponents/TwitterCardMobile';
import Gallery from './customComponents/TwitterGallery';
import styles from './FeedTwitterStyles';

import Button from 'material-ui/Button';

const propTypes = {
    tweets: PropTypes.array,
    moduleColor: PropTypes.string,
    miniMode: PropTypes.bool,
    onClickedAction: PropTypes.func,
    labelAction: PropTypes.string
};

const defaultProps = {
    tweets: [],
    moduleColor: '#666',
    miniMode: false,
    onClickedAction: () => {}
};

class FeedTwitterMobile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: props.tweets,
            scrollActive: false
        };
        this.clickedButton = this.clickedButton.bind(this);
    }

    componentDidMount() {
        const feed = ReactDOM.findDOMNode(this);
        const feedTweets = feed.getElementsByClassName('feed-container-list')[0];
        const isUserScrollingDown = positions => (
            positions[0].sT < positions[1].sT
        );

        const isScrollExpectedPercent = (position, percent) =>
            ((position.sT + position.cH) / position.sH) > (percent / 100)
        ;
    }

    componentWillReceiveProps(nextProps) {
        //console.log(nextProps);
    }

    clickedButton(tweet) {
        this.props.onClickedAction(tweet);
    }
    
    renderTweet(tweet, index) {
        return (
            <div key={index}>
                <TwitterCardMobile {...tweet} key={index}>
                    <Gallery items={tweet._source.images} />
                </TwitterCardMobile>
                <Button
                    raised
                    color="primary"
                    style={{width: '100%', height: 40, marginTop: -10, marginBottom: 15}}
                    onClick={() => {
                        this.clickedButton(tweet);
                    }}
                >
                    {this.props.labelAction}
                </Button>
            </div>
        );
    }

    render() {
        const { moduleColor, miniMode } = this.props;
        const { tweets } = this.props;
        return (
            <div className="feed-twitter" {...styles(moduleColor)}>
                <div className="feed-container-list">
                    {(() => {
                        if (miniMode) {
                            return this.renderTweet(tweets[0], 0);
                        }
                        return tweets.map(
                            (tweet, index) => (this.renderTweet(tweet, index))
                        );
                    })()}
                </div>
            </div>
        );
    }
}

FeedTwitterMobile.propTypes = propTypes;
FeedTwitterMobile.defaultProps = defaultProps;

export default FeedTwitterMobile;

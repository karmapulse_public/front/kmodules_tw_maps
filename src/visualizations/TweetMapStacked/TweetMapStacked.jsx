/*
*   VISUALIZACIÓN TWEET MAPS
*  --------------------------
*   Este componente renderea la data que le ha llegado.
*   En la mayoría de los casos, es un componente stateless.
*/
// import 'mapbox-gl/dist/mapbox-gl.css';
import moment from 'moment';
import Geohash from 'latlon-geohash';
import React, { Component } from 'react';
import MapGL, { Marker } from 'react-map-gl';
// import DeckGL, { IconLayer, ScreenGridLayer } from 'deck.gl';
import PropTypes from 'prop-types';
import { geolocated } from 'react-geolocated';
import { css } from 'glamor';
import fetchXHR from '../../common/utils';

import mapboxStyles from './mapbox-gl';

css.insert(mapboxStyles);

const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoiZ2FyZmlhc2xvcGV6IiwiYSI6ImNqY256OHB4NjFvcHEycG1yMGJkZmRlbzMifQ.wK2AyBjgxdqvbxbloe_gjg';

class TweetMapStacked extends Component {
    constructor(props) {
        super(props);
        this.defaultZoom = 11;
        this.mapConfig = {
            key: 'AIzaSyBArcwzTXl5DINP26zbIEv0a_kclyXykyo',
            language: 'mx',
            region: 'mx'
        };
        this.state = {
            totalTweets: 0,
            geoZones: [],
            viewport: {
                width: 700,
                height: 700,
                latitude: 37.7577,
                longitude: -122.4376,
                zoom: 10
            }
        };

        this.onViewportChange = this.onViewportChange.bind(this);
        this.setWeigthColor = this.setWeigthColor.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.geohashSelected = this.geohashSelected.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            if (nextProps.coords) {
                const coords = {
                    lat: nextProps.coords.latitude,
                    lon: nextProps.coords.longitude
                }
                const request = {
                    initialDate: moment().toISOString,
                    finalDate: moment().toISOString,
                    module_id: 'module_tw_map',
                    module_filters: [
                        {
                            field: 'geo',
                            value: coords
                        }
                    ]
                };
                fetchXHR('https://e2eir9aep2.execute-api.us-west-2.amazonaws.com/dev/query_recipe_twitter', 'POST', request).then((response) => {
                    const total = response.json.data.total;
                    const parsed = response.json.data.map((geoObj) => {
                        const pos = Geohash.decode(geoObj.key);
                        const centroidLat = ((
                            geoObj.cell.bounds.top_left.lat +
                            geoObj.cell.bounds.bottom_right.lat) / 2
                        );
                        const centroidLon = ((
                            geoObj.cell.bounds.top_left.lon +
                            geoObj.cell.bounds.bottom_right.lon) / 2
                        );
                        return {
                            total: geoObj.doc_count,
                            centroid: [centroidLon, centroidLat],
                            position: [centroidLon, centroidLat],
                            geohash: geoObj.key
                        };
                    });
                    const newVp = this.state.viewport;
                    newVp.latitude = coords.lat;
                    newVp.longitude = coords.lon;
                    this.setState({
                        totalTweets: total,
                        geoZones: parsed,
                        viewport: newVp
                    });
                });
            }
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    onViewportChange(viewport) {
        console.log(viewport.zoom);
        if (viewport.zoom < 12) {
            console.log("menor a 12");
        }
        this.setState({
            viewport: { ...this.state.viewport, ...viewport }
        });
    }
    setWeigthColor(obj) {
        const w = ((obj.total * 100) / (this.state.totalTweets / 10));
        return w;
    }

    updateWindowDimensions() {
        this.setState({
            viewport: {
                ...this.state.viewport,
                ...{ width: window.innerWidth, height: window.innerHeight
                }
            }
        });
    }

    geohashSelected(obj) {
        console.log(obj);
    }

    render() {
        if (this.props.coords) {
            if (this.state.geoZones.length > 0) {
                const initialLocation = {
                    lat: this.props.coords.latitude,
                    lng: this.props.coords.longitude
                };
                const ICON_MAPPING = {
                    marker: { x: 0, y: 0, width: 128, height: 128, mask: true }
                };
                const Markers = this.state.geoZones.map((m) => (
                    <Marker
                        latitude={m.position[1]}
                        longitude={m.position[0]}
                        offsetLeft={-20}
                        offsetTop={-10}
                        key={m.geohash}
                        captureClick
                    >
                        <div
                            onClick={() => {
                                this.geohashSelected(m);
                            }}
                            role="presentation"
                        >
                            <LocationIcon
                                total={m.total}
                            />
                        </div>
                    </Marker>
                ));
                return (
                    <MapGL
                        {...this.state.viewport}
                        mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}
                        onViewportChange={this.onViewportChange}
                    >
                        {Markers}
                    </MapGL>
                );
            }
            return (
                <div>
                    Getting zones...
                </div>
            );
        }
        return (
            <div>
                Getting coordinates...
            </div>
        );
    }
}

TweetMapStacked.defaultProps = {
    coords: PropTypes.object
};

const LocationIcon = ({ total }) => (
    <svg
        width="35px"
        height="46px"
        viewBox="0 0 35 46"
        version="1.1"
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
            <path
                d="M17.5 0C7.825 0 0 7.199 0 16.1 0 28.175 17.5 46 17.5 46S35 28.175 35 16.1C35 7.199 27.175 0 17.5 0z"
                fill="#000"
                fillRule="nonzero"
            />
            <text
                fontFamily="Roboto-Regular, Roboto"
                fontSize={9}
                fontWeight="normal"
                fill="#FFF"
                textAnchor="middle"
            >
                <tspan x="50%" y="50%">
                    {total}
                </tspan>
            </text>
            <polygon points="0 0 35 0 35 46 0 46" />
        </g>
    </svg>
);

export default geolocated({
    positionOptions: {
        enableHighAccuracy: false,
    },
    userDecisionTimeout: 5000,
})(TweetMapStacked);

import { css } from 'glamor';

const styles = (mainColor, alignment) => css({
    fontSize: 16,
    fontFamily: 'inherit',
    color: mainColor || '#FFF',
    textAlign: alignment
});

export default styles;

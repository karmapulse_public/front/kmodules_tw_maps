import { css } from 'glamor';

const styles = (mainColor, alignment) => css({
    fontSize: 16,
    color: mainColor || '#FFF',
    textAlign: alignment
});

export default styles;

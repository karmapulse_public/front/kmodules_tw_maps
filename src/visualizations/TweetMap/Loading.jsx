/*
*   LOADING
*  --------------------------
*   Este componente aparece cuando se está cargando la data.
*   Cada visualización tiene su propio "Loading", porque
*   es un wireframe dummy de lo que se verá en pantalla.
*/

import React from 'react';

import styles from './styles';

export default () => (
    <div {...styles('plum', 'center')}>
        Cargando Visualización...
    </div>
);

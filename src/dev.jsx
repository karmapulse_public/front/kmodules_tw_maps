import React from 'react';
import { render } from 'react-dom';
import TwitterMap from './TwitterMap';
import MapConnect from './MapConnect';
import fetchXHR from './common/utils';

const selectedCluster = (cluster) => {
    console.log("Selected cluster");
    console.log(cluster);

    const query = {
        recipe_id: 'module_tw_feed',
        filters: {
            bounding_box: cluster.bounding_box
        }
    };
    fetchXHR('https://e2eir9aep2.execute-api.us-west-2.amazonaws.com/dev/query_recipe_twitter', 'POST', query).then((response) => {
        console.log(response);
    });
};

const MyApp = () => (
    <MapConnect
        module_id="maptweetdemo"
        access_token="karma_team_abc"
    >
        <TwitterMap
            onClusterSelected={selectedCluster}
        />
    </MapConnect>
);

render(<MyApp />, document.getElementById('root'));

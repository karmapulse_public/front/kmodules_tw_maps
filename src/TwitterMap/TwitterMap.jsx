import React from 'react';
import PropTypes from 'prop-types';

import {
    TweetMap,
    TweetMapGrid,
    TweetMapStacked
} from 'visualizations';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {
        visualization: 'tweet_map'
    },
    moduleData: {},
    moduleColor: '#555'
};

const TwitterMap = (props) => {
    const renderByViz = () => {
        const { moduleConfig } = props;
        if (moduleConfig) {
            const visualizations = {
                tweet_map: params => (
                    <TweetMap {...params} />
                ),
                tweet_map_grid: params => (
                    <TweetMapGrid {...params} />
                ),
                tweet_map_stacked: params => (
                    <TweetMapStacked {...params} />
                )
            };

            return visualizations[moduleConfig.visualization](props);
        }
        return 'No tengo configuración :(';
    };

    return (
        <div {...styles()}>
            {renderByViz()}
        </div>
    );
};

TwitterMap.propTypes = propTypes;
TwitterMap.defaultProps = defaultProps;

export default TwitterMap;

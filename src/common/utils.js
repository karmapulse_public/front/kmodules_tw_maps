export default function fetchXHR(url, method, data) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();

        request.open(method, url, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
        request.send(JSON.stringify(data));

        request.onreadystatechange = function handleResponse() {
            if ((this.responseText !== undefined && this.responseText !== ''
                && this.responseText !== null && this.responseText !== ' '
                && this.responseText[this.responseText.length - 1] === '}'
                && this.readyState === 4 && this.status === 200)
                || (this.readyState === 4 && this.status === 400)) {
                const response = {
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                };
                resolve(response);
            }
        };
        request.onerror = function handleError(error) {
            reject(error);
        };
    });
}

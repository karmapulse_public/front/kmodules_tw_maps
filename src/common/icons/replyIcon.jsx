import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    color: PropTypes.string
};

const defaultProps = {
    color: '#FFF'
};

const ReplyIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">
        <path fill={props.color} fillRule="evenodd" d="M12.973 4.904C10.72 3.258 7.836 2.937 6.972 2.911V1.424c0-.8-.74-1.127-1.451-.727L.553 3.446c-.71.4-.75 1.055-.04 1.455l5.027 2.75c.711.399 1.432.071 1.432-.729V5.406c.864.014 2.76.241 4.445 1.474 2.467 1.801 2.31 4.965 2.308 4.991-.029.697.447 1.221 1.161 1.221h.056c.69 0 1.344-.439 1.373-1.117.008-.185.232-4.46-3.342-7.07"/>
    </svg>
    /* eslint-enable */
);

ReplyIcon.propTypes = propTypes;
ReplyIcon.defaultProps = defaultProps;

export default ReplyIcon;

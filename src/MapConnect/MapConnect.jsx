/*
*   CONECTOR RECIPE DE MODULO
*  --------------------------
*   Este componente se encarga de recuperar la configuracion del modulo y de recupera la DATA.
*   module_id   -> Con este campo recupera la informacion
*   access_token -> Con este campo validamos que este modulo sea valido.
*
*   Recupera de configuracion
*     -> data_source
*           - denomination
*           - rule_id
*           - url
*     -> visualization
*     -> recipe_id
*     -> layout
*           - colors
*           - labels
*           - fonts
*
*
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetchXHR from '../common/utils';

class MapConnect extends Component {

    constructor(props) {
        super(props);
        let status = 'Initialized';
        if (props.access_token !== undefined) {
            if (props.module_id !== undefined) {
                status = 'Initialized';
            } else {
                status = 'Missing module_id';
            }
        } else {
            status = 'Missing access_token';
        }
        this.state = {
            status,
            config: {},
            data: []
        };
        this.loadConfig = this.loadConfig.bind(this);
        this.loadData = this.loadData.bind(this);
        this.updateData = this.updateData.bind(this);
    }
    componentDidMount() {
        this.loadConfig();
    }
    loadConfig() {
        this.setState({
            status: 'Loading Config'
        });
        // TODO: Replace for FETCH REQUEST TO API.
        setTimeout(() => {
            const configuration = {
                data_source: {
                    denomination: 'twitter',
                    rule_id: 'd4cde63a8edc4693b125beb6b77c7da4',
                    url: 'https://e2eir9aep2.execute-api.us-west-2.amazonaws.com/dev/query_recipe_twitter'
                },
                visualization: 'tweet_map',
                recipe_id: 'module_tw_map',
                query: {
                    initial_date: '2017-01-01T00:00:00.000Z',
                    final_date: '2018-01-31T00:00:00.000Z',
                    filters: {
                        geo: {
                            value: {
                                lat: 37.7577,
                                lon: -122.4376
                            }
                        },
                        precision: {
                            value: 4
                        },
                        distance: {
                            value: '20km'
                        }
                    }
                },
                options: {
                    withLocalization: true,
                    layout: {
                        colors: [],
                        labels: [],
                        fonts: []
                    }
                }

            };
            this.setState({
                status: 'Loading Data',
                config: configuration
            });
            this.loadData(configuration);
        }, 1000);
    }
    loadData(configuration) {
        this.setState({
            status: 'Loading Data'
        });
        const query = {
            ...configuration.query,
            rule_id: configuration.data_source.rule_id,
            recipe_id: configuration.recipe_id,
        };
        fetchXHR(configuration.data_source.url, 'POST', query).then((response) => {
            this.setState({
                status: 'OK',
                data: response.json
            });
        });
    }
    updateData(newRequest) {
        const query = {
            ...this.state.config.query,
            filters: {
                ...this.state.config.query.filters,
                ...newRequest
            },
            rule_id: this.state.config.data_source.rule_id,
            recipe_id: this.state.config.recipe_id
        };
        const lastConfig = this.state.config;
        lastConfig.query = {
            ...this.state.config.query,
            filters: {
                ...this.state.config.query.filters,
                ...newRequest
            }
        };
        fetchXHR(this.state.config.data_source.url, 'POST', query).then((response) => {
            this.setState({
                status: 'OK',
                data: response.json,
                config: lastConfig
            });
        });
    }
    render() {
        const { status } = this.state;
        const { children } = this.props;
        switch (status) {
        case 'Loading Config': {
            return (
                <div> Loading Config... </div>
            );
        }
        case 'Loading Data': {
            return (
                <div> Loading data... </div>
            );
        }
        case 'OK': {
            const childrenWithProps = React.Children.map(children, child =>
                React.cloneElement(child, {
                    moduleConfig: this.state.config,
                    moduleData: this.state.data,
                    updateData: this.updateData
                })
            );
            return (
                <div>{childrenWithProps}</div>
            );
        }
        default:
            return (
                <div> {status} </div>
            );
        }
    }
}

MapConnect.defaultProps = {
    module_id: 'maptweetdemo',
    access_token: 'karma_team_abc',
    initial_values: {},
    children: {}
};

MapConnect.propTypes = {
    module_id: PropTypes.string,
    access_token: PropTypes.string,
    children: PropTypes.object
};

export default MapConnect;
